package uk.co.objectivity.zjava.reactiveexampels.domain;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CounterDto {

  private final Counter counter;
  private final String localDateTime;
}
