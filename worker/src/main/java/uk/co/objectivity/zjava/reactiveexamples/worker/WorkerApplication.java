package uk.co.objectivity.zjava.reactiveexamples.worker;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.print.attribute.standard.Media;

@SpringBootApplication
@EnableWebFlux
@Slf4j
public class WorkerApplication {

  public static void main(String[] args) {
    SpringApplication.run(WorkerApplication.class, args);
  }


  @RestController
  @RequestMapping("worker")
  public class WorkerController {

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public void doWork(@RequestParam Integer port, @RequestParam Boolean stream,
        @RequestParam Integer limit, @RequestParam String path) {
      WebClient.create("http://localhost:" + port).get().uri(
          uriBuilder -> uriBuilder.path("counters").path('/' + path)
              .queryParam("limit", limit)
              .queryParam("stream", stream).build())
          .headers(httpHeaders -> httpHeaders
              .set(HttpHeaders.ACCEPT, stream ? MediaType.APPLICATION_STREAM_JSON_VALUE
                  : MediaType.APPLICATION_JSON_VALUE)).retrieve()
          .bodyToFlux(String.class)
          .index()
          .log()
          .subscribe();
    }

    @GetMapping(value = "zipped")
    public Flux<String> zip() {
      Flux<String> relational = WebClient
          .create("http://localhost:8082/counters")
          .get()
          .uri(uriBuilder -> uriBuilder.queryParam("limit", "10").build())
          .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_STREAM_JSON_VALUE)
          .exchange()
          .flux()
          .flatMap(clientResponse -> clientResponse.bodyToFlux(String.class));

      Flux<String> mongo = WebClient
          .create("http://localhost:8081/counters")
          .get()
          .uri(uriBuilder -> uriBuilder.queryParam("limit", "10").build())
          .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_STREAM_JSON_VALUE)
          .exchange()
          .flux()
          .flatMap(clientResponse -> clientResponse.bodyToFlux(String.class));

      return Flux.combineLatest(relational, mongo, (s, s2) -> "Relational: " + s + " Mongo: " + s2).log();
    }

  }
}
