import {ChangeDetectorRef, Component} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {FormControl} from '@angular/forms';
import {EMPTY, Observable, range} from 'rxjs';
import {catchError, finalize, map, reduce, tap} from 'rxjs/operators';


interface Panel {
  text: string;
  url: string;
  isLoading: boolean;
  eventSource: boolean;
}

const panels: Panel[] = [
  {text: 'Postgres', url: 'http://localhost:8080/counters', isLoading: false, eventSource: false},
  {
    text: 'Postgres eventStream',
    url: 'http://localhost:8080/counters/event',
    isLoading: false,
    eventSource: true
  },
  {
    text: 'Postgres eventStream scheduled',
    url: 'http://localhost:8080/counters/event-scheduled',
    isLoading: false,
    eventSource: true
  },
  {
    text: 'Postgres flux',
    url: 'http://localhost:8080/counters/event',
    isLoading: false,
    eventSource: false
  },
  {
    text: 'Postgres flux scheduled',
    url: 'http://localhost:8080/counters/event-scheduled',
    isLoading: false,
    eventSource: false
  },
  {
    text: 'Postgres reactive',
    url: 'http://localhost:8082/counters',
    isLoading: false,
    eventSource: false
  },
  {
    text: 'Postgres reactive eventStream',
    url: 'http://localhost:8082/counters',
    isLoading: false,
    eventSource: true
  },
  {
    text: 'Mongo eventStream',
    url: 'http://localhost:8081/counters',
    isLoading: false,
    eventSource: true
  },
  {text: 'Mongo', url: 'http://localhost:8081/counters', isLoading: false, eventSource: false},
];

interface Data {
  counter: { id: number, name: string };
  localDateTime: Date;
  accumulator: number;
}

const reducer = (acc, value) => {
  let find = acc.find(val => val.localDateTime === value.localDateTime);
  if (!find) {
    find = value;
    find.accumulator = 0;
    acc.push(find);
  }
  find.accumulator = ++find.accumulator;
  return acc;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  panels: Panel[] = panels;

  data$: Observable<Data[]>[] = [];
  stream: Data[] = [];
  limit: FormControl;
  iterations: FormControl;
  source: EventSource[] = [];

  constructor(private httpClient: HttpClient, private cd: ChangeDetectorRef) {
    this.cleanControls();
  }

  private call(panel: Panel): void {
    panel.isLoading = true;
    if (panel.eventSource) {
      this.eventCall(panel);
    } else {
      this.httpCall(panel);
    }
  }

  private httpCall(panel: Panel) {
    range(1, this.iterations.value).pipe(tap(it => {
          this.data$[it] = this.httpClient.get<Data[]>(panel.url, this.buildOptions())
              .pipe(
                  catchError(() => {
                    panel.isLoading = false;
                    return EMPTY;
                  }),
                  finalize(() => panel.isLoading = false),
                  map((data: Data[]) => data.reduce(reducer, [])));
        }
    ))
        .subscribe();
  }

  eventCall(panel: Panel): void {
    range(0, this.iterations.value).pipe(tap(it => {
          this.source[it] = new EventSource(panel.url + '?limit=' + this.limit.value);
          this.source[it].onmessage = evt => {
            const usedLimit = this.getUsedLimit();
            if (usedLimit >= this.limit.value) {
              this.closeStream(panel, it);
              return;
            }
            if (it === 0) {
              const d = JSON.parse(evt.data);
              this.stream = reducer(this.stream, d);
              if (usedLimit % 2 === 0) {
                this.cd.detectChanges();
              }
            }
          };
          this.source[it].onerror = evt => this.closeStream(panel, it);
        }
    )).subscribe();
  }

  getUsedLimit() {
    return this.stream.reduce((previousValue, currentValue) => previousValue + currentValue.accumulator, 0);
  }

  private closeStream(panel: Panel, it: number) {
    panel.isLoading = false;
    if (this.source) {
      this.source[it].close();
      this.source[it] = null;
    }
  }

  private opened(): void {
    this.cleanControls();
    this.data$ = [];
    this.stream = [];
    this.cleanStreams();
    this.panels.forEach(panel => panel.isLoading = false);
  }

  private cleanControls() {
    this.limit = new FormControl();
    this.iterations = new FormControl();
  }

  private cleanStreams() {
    this.source.filter(value => !!value).forEach(value => value.close());
    this.source = [];
  }

  private buildOptions() {
    return {
      params: new HttpParams().append('limit', this.limit.value)
    };
  }

  private stop(panel: Panel): void {
    if (panel.eventSource) {
      panel.isLoading = false;
      this.source.forEach(value => value.close());
    } else {
      panel.isLoading = false;
      this.data$ = [];
    }
  }
}
