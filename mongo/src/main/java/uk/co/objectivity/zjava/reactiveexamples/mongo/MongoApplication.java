package uk.co.objectivity.zjava.reactiveexamples.mongo;

import static reactor.core.publisher.Flux.range;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;
import uk.co.objectivity.zjava.reactiveexamples.mongo.counter.Counter;
import uk.co.objectivity.zjava.reactiveexamples.mongo.counter.CounterRepository;

@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableWebFlux
@AllArgsConstructor
public class MongoApplication implements CommandLineRunner {

  private final CounterRepository counterRepository;

  public static void main(String[] args) {
    SpringApplication.run(MongoApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    counterRepository
        .count()
        .filter(aLong -> aLong == 0)
        .flux()
        .flatMap(count ->
            range(1, 1_000_000)
                .map(integer -> Counter.builder()
                    .name("mongo" + integer)
                    .build())
                .buffer(1000)
                .flatMap(counterRepository::saveAll))
        .subscribe();
  }
}
