package uk.co.objectivity.zjava.reactiveexamples.mongo.counter;

import java.math.BigInteger;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "counters")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Counter {

  @Id
  private BigInteger id;
  private String name;
}
