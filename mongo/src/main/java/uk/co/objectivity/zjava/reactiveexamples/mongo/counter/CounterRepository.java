package uk.co.objectivity.zjava.reactiveexamples.mongo.counter;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface CounterRepository extends ReactiveCrudRepository<Counter, Long> {

}
