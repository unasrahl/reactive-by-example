package uk.co.objectivity.zjava.reactiveexamples.mongo.counter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("counters")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class CounterController {

  private final CounterRepository counterRepository;

  @GetMapping
  public Flux<CounterDto> countersStream(
      @RequestParam(value = "limit", required = false, defaultValue = "100000") Integer limit) {
    return counterRepository.findAll()
        .limitRequest(limit)
        .map(counter -> CounterDto.builder()
            .counter(counter)
            .localDateTime(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
            .build())
        .delayElements(Duration.ofSeconds(1L))
        .log();
  }
}
