package uk.co.objectivity.zjava.reactiveexamples.postgres.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uk.co.objectivity.zjava.reactiveexampels.domain.Counter;

@Repository
public interface CounterRepository extends JpaRepository<Counter, Long> {

}
