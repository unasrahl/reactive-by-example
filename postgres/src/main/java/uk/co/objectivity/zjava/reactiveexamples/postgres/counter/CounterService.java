package uk.co.objectivity.zjava.reactiveexamples.postgres.counter;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import uk.co.objectivity.zjava.reactiveexampels.domain.Counter;
import uk.co.objectivity.zjava.reactiveexamples.postgres.repository.CounterRepository;

@Service
@AllArgsConstructor
public class CounterService {

  private final CounterRepository counterRepository;

  @Qualifier("jdbcScheduler")
  private final Scheduler scheduler;

  public Flux<Counter> getCountersFluxWithScheduler(Integer limit) {
    return getCountersFlux(limit)
        .publishOn(scheduler);
  }

  public Flux<Counter> getCountersFlux(Integer limit) {
    return Mono
        .fromCallable(() -> counterRepository
            .findAll(PageRequest.of(0, limit))
            .getContent())
        .flux()
        .flatMap(Flux::fromIterable);
  }

  public Page<Counter> getCounters(Integer limit) {
    return counterRepository.findAll(PageRequest.of(0, limit));
  }
}
