package uk.co.objectivity.zjava.reactiveexamples.postgres.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import uk.co.objectivity.zjava.reactiveexampels.domain.CounterDto;
import uk.co.objectivity.zjava.reactiveexamples.postgres.counter.CounterService;

@RestController
@RequestMapping("counters")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class CounterController {

  private final CounterService counterService;

  @GetMapping
  public List<CounterDto> counters(
      @RequestParam(value = "limit", required = false, defaultValue = "100000") Integer limit) {
    return counterService
        .getCounters(limit)
        .map(counter -> CounterDto.builder()
            .counter(counter)
            .localDateTime(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
            .build())
        .getContent();
  }

  @GetMapping(value = "event")
  public Flux<CounterDto> countersStream(
      @RequestParam(value = "limit", required = false, defaultValue = "100000") Integer limit) {
    return counterService
        .getCountersFlux(limit)
        .map(counter -> CounterDto.builder()
            .counter(counter)
            .localDateTime(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
            .build())
        .log();
  }

  @GetMapping(value = "event-scheduled")
  public Flux<CounterDto> countersStreamWithScheduler(
      @RequestParam(value = "limit", required = false, defaultValue = "100000") Integer limit) {
    return counterService
        .getCountersFluxWithScheduler(limit)
        .map(counter -> CounterDto.builder()
            .counter(counter)
            .localDateTime(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
            .build())
        .log();
  }
}
