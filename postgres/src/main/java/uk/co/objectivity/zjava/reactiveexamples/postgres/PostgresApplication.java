package uk.co.objectivity.zjava.reactiveexamples.postgres;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.reactive.config.EnableWebFlux;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import uk.co.objectivity.zjava.reactiveexampels.domain.Counter;
import uk.co.objectivity.zjava.reactiveexamples.postgres.repository.CounterRepository;

@AllArgsConstructor
@SpringBootApplication
@EnableWebFlux
@EntityScan(basePackages = {"uk.co.objectivity.zjava.reactiveexampels.domain"})
public class PostgresApplication implements CommandLineRunner {

  private CounterRepository counterRepository;

  public static void main(String[] args) {
    SpringApplication.run(PostgresApplication.class, args);
  }

  @Override
  public void run(String... args) {
    if (counterRepository.count() == 0) {
      Flux.range(1, 1_000_000)
          .map(
              integer -> Counter.builder().id(Long.valueOf(integer)).name("test" + integer).build())
          .buffer(10_000)
          .map(counters -> counterRepository.saveAll(counters))
          .subscribe();
    }
  }
}
