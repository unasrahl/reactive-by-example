package uk.co.objectivity.zjava.reactiveexamples.postgresreactive.configuration;

import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.r2dbc.function.DatabaseClient;
import org.springframework.data.r2dbc.repository.support.R2dbcRepositoryFactory;
import org.springframework.data.relational.core.mapping.RelationalMappingContext;
import uk.co.objectivity.zjava.reactiveexamples.postgresreactive.counter.CounterRepository;

@Configuration
public class R2dbcConfig {

  @Bean
  public PostgresqlConnectionFactory connectionFactory() {
    return new PostgresqlConnectionFactory(PostgresqlConnectionConfiguration.builder()
        .host("localhost")
        .database("postgres")
        .port(5432)
        .username("postgres")
        .password("postgres")
        .build());
  }

  @Bean
  public DatabaseClient databaseClient(PostgresqlConnectionFactory connectionFactory) {
    return DatabaseClient.create(connectionFactory);
  }

  @Bean
  public MappingContext mappingContext() {
    final RelationalMappingContext relationalMappingContext = new RelationalMappingContext();
    relationalMappingContext.afterPropertiesSet();
    return relationalMappingContext;
  }

  @Bean
  public R2dbcRepositoryFactory repositoryFactory(DatabaseClient databaseClient,
      MappingContext mappingContext) {
    return new R2dbcRepositoryFactory(databaseClient, mappingContext);
  }

  @Bean
  public CounterRepository reactiveCoffeeRepository(R2dbcRepositoryFactory repositoryFactory) {
    return repositoryFactory.getRepository(CounterRepository.class);
  }
}
