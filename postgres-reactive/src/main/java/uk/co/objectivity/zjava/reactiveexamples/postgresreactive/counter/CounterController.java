package uk.co.objectivity.zjava.reactiveexamples.postgresreactive.counter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import uk.co.objectivity.zjava.reactiveexampels.domain.CounterDto;

@RestController
@RequestMapping("counters")
@AllArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class CounterController {

  private final CounterService counterService;

  @GetMapping
  public Flux<CounterDto> counters(
      @RequestParam(value = "limit", required = false, defaultValue = "100000") Integer limit) {
    return counterService.getCountersStream(limit)
        .map(counter -> CounterDto.builder()
            .counter(counter)
            .localDateTime(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME))
            .build());
  }
}
