package uk.co.objectivity.zjava.reactiveexamples.postgresreactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication
@EnableWebFlux
public class PostgresReactiveApplication {

  public static void main(String[] args) {
    SpringApplication.run(PostgresReactiveApplication.class, args);
  }
}
