package uk.co.objectivity.zjava.reactiveexamples.postgresreactive.counter;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import uk.co.objectivity.zjava.reactiveexampels.domain.Counter;

import java.time.Duration;

@Service
@AllArgsConstructor
public class CounterService {

  private final CounterRepository counterRepository;

  Flux<Counter> getCountersStream(Integer limit) {
    return counterRepository.findAll()
        .limitRequest(limit)
        .delayElements(Duration.ofMillis(100))
        .log();
  }
}
