package uk.co.objectivity.zjava.reactiveexamples.postgresreactive.counter;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import uk.co.objectivity.zjava.reactiveexampels.domain.Counter;

public interface CounterRepository extends ReactiveCrudRepository<Counter, Long> {

}
